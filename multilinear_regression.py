import numpy as np 
from cheml.datasets import load_qm7
import tensorflow as tf
from datetime import datetime

def llin(x,shape,dtype='float32',initializer=tf.random_uniform_initializer(0,1e-2)):
	pass

class tfmodel(object):
	
	def __init__(self,N,logname,name='tfmodel', epochs=10000, batchsize=128,target_scale=1,target_offset=0,optimizer='adagrad',
			patience=500,n_folds=5,n_l0=300,n_l1=None,use_nesterov=True, n_l2=None, lr=None, M=None, A=None, B=None, use_diag_penalty=False,
			use_dropout=True,use_l2=False,orders='1234'):
		with tf.device('/device:GPU:0'):
			self.N=N
			self.n_folds=n_folds
			self.use_diag_penalty=use_diag_penalty
			self.gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=1/2.5)
			tf.reset_default_graph()

			self.logname = logname
			self.use_l2 = use_l2
			self.use_dropout=use_dropout
			self.orders=orders
			self.patience = patience
			self.epochs = epochs
			self.n_l0 = n_l0
			self.n_l1 = n_l1
			self.lr = lr
			self.M = M
			self.A = A
			self.optimizer = optimizer
			self.use_nesterov = use_nesterov
			self.B = B
			self.name = name
			self.batchsize = batchsize
			self.target_scale = target_scale
			self.target_offset = target_offset

			# Computational graph definition
			################################
			self.tX = tf.placeholder(tf.float32,name='X', shape=[None, self.n_l0])
			self.tY = tf.placeholder(tf.float32,name='Y', shape=[None,1])
			# self.phase= tf.placeholder(tf.bool,name='phase')
			with tf.variable_scope("layer_1"):
				upbound=np.sqrt(6/(self.n_l0+n_l1**2))

				if self.orders.find('1')>-1:
					print("Order {} prepared".format(1))

					# self.tW11 = tf.get_variable("W11",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					# self.tb11 = tf.get_variable("b11",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))

					# self.tZ11 = tf.matmul(self.tX, self.tW11) + self.tb11
					self.tZ11 = self.tX#, self.tW11) + self.tb11

				if self.orders.find('2')>-1:
					print("Order {} prepared".format(2))

					self.tW21 = tf.get_variable("W21",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb21 = tf.get_variable("b21",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW22 = tf.get_variable("W22",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb22 = tf.get_variable("b22",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))

					self.tZ21 = (tf.matmul(self.tX, self.tW21) + self.tb21)*(tf.matmul(self.tX, self.tW22) + self.tb22)#*(tf.matmul(self.tX, self.tW13) + self.tb13)

				upbound2=np.sqrt(6/(self.n_l1)**2)

				if self.orders.find('3')>-1:
					print("Order {} prepared".format(3))
					self.tW31 = tf.get_variable("W31",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb31 = tf.get_variable("b31",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW32 = tf.get_variable("W32",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb32 = tf.get_variable("b32",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW33 = tf.get_variable("W33",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb33 = tf.get_variable("b33",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
				
					self.tZ31 = (tf.matmul(self.tX, self.tW31) + self.tb31)*(tf.matmul(self.tX, self.tW32) + self.tb32)*(tf.matmul(self.tX, self.tW33) + self.tb33)

				if self.orders.find('4')>-1:
					print("Order {} prepared".format(4))
					self.tW41 = tf.get_variable("W41",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb41 = tf.get_variable("b41",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW42 = tf.get_variable("W42",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb42 = tf.get_variable("b42",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW43 = tf.get_variable("W43",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb43 = tf.get_variable("b43",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW44 = tf.get_variable("W44",shape=[self.n_l0, self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb44 = tf.get_variable("b44",shape=[self.n_l1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
				
					self.tZ41 = (tf.matmul(self.tX, self.tW41) + self.tb41)*(tf.matmul(self.tX, self.tW42) + self.tb42)*(tf.matmul(self.tX, self.tW43) + self.tb43)*(tf.matmul(self.tX, self.tW44) + self.tb44)

			with tf.variable_scope("layer_2"):
				self.tZ=None
				self.pnlt=None
				self.dpenalty=None
				if self.orders.find('1')>-1:
					print("Order {} included".format(1))
					# self.tW1 = tf.get_variable("W1",shape=[self.n_l1, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tW1 = tf.get_variable("W1",shape=[self.n_l0, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tb1 = tf.get_variable("b1",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound))
					self.tZ1 = tf.matmul(self.tZ11, self.tW1) + self.tb1
					
					self.tZ = self.tZ1 if self.tZ is None else self.tZ1 +self.tZ
					
					if self.use_l2:
						self.pnlt= tf.reduce_mean(tf.square(self.tW1)) if self.pnlt is None else self.pnlt + tf.reduce_mean(tf.square(self.tW1))
						# self.pnlt += tf.reduce_mean(tf.square(self.tW11))
						# self.pnlt += tf.reduce_mean(tf.square(self.tb11))
						self.pnlt += tf.reduce_mean(tf.square(self.tb1))
						# print("O")

				if self.orders.find('2')>-1:
					print("Order {} included".format(2))
					self.tW2 = tf.get_variable("W2",shape=[self.n_l1, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					self.tb2 = tf.get_variable("b2",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					# self.tZ21_new = tf.reduce_sum(tf.squeeze(tf.reshape(self.tZ21,(-1,int(self.n_l1/2),2))),2)
					self.tZ2 = tf.matmul(self.tZ21, self.tW2) + self.tb2
					to_diag = tf.matmul(tf.transpose(self.tW21),self.tW22)
					# self.tW2 = tf.get_variable("W2",shape=[self.n_l1/2, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					# self.tb2 = tf.get_variable("b2",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					# self.tZ21_new = tf.reduce_sum(tf.squeeze(tf.reshape(self.tZ21,(-1,int(self.n_l1/2),2))),2)
					# self.tZ2 = tf.matmul(self.tZ21_new, self.tW2) + self.tb2

					# self.tW2 = tf.get_variable("W2",shape=[self.n_l1/2, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					# self.tb2 = tf.get_variable("b2",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					# self.tZ21_new = tf.reduce_sum(tf.squeeze(tf.reshape(self.tZ21,(-1,int(self.n_l1/2),2))),2)
					# self.tZ2 = tf.matmul(self.tZ21_new, self.tW2) + self.tb2

					self.tZ = self.tZ2 if self.tZ is None else self.tZ2 +self.tZ
					
					if self.use_l2:
						self.pnlt = tf.reduce_mean(tf.square(self.tW21)) if self.pnlt is None else  self.pnlt + tf.reduce_mean(tf.square(self.tW21))
						self.pnlt += tf.reduce_mean(tf.square(self.tb21))
						self.pnlt += tf.reduce_mean(tf.square(self.tW22))
						self.pnlt += tf.reduce_mean(tf.square(self.tb22))
						# self.pnlt += tf.reduce_mean(tf.square(self.tW2))
						self.pnlt2 = tf.reduce_mean(tf.square(self.tW2)) if self.pnlt is None else  self.pnlt + tf.reduce_mean(tf.square(self.tW2))
						self.pnlt2 += tf.reduce_mean(tf.square(self.tb2))
					if self.use_diag_penalty:
						self.D2=tf.diag(tf.get_variable('D2',shape=[self.n_l1,],dtype='float32'))
						self.dpenalty = tf.reduce_sum((self.D2-to_diag)**2)

				if self.orders.find('3')>-1:
					print("Order {} included".format(3))
					self.tW3 = tf.get_variable("W3",shape=[self.n_l1, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					self.tb3 = tf.get_variable("b3",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					self.tZ3 = tf.matmul(self.tZ31, self.tW3) + self.tb3
					self.tZ = self.tZ3 if self.tZ is None else self.tZ3 +self.tZ

					if self.use_l2:
						self.pnlt = tf.reduce_mean(tf.square(self.tW31)) if self.pnlt is None else self.pnlt + tf.reduce_mean(tf.square(self.tW31))
						self.pnlt += tf.reduce_mean(tf.square(self.tb31))
						self.pnlt += tf.reduce_mean(tf.square(self.tW32))
						self.pnlt += tf.reduce_mean(tf.square(self.tb32))
						self.pnlt += tf.reduce_mean(tf.square(self.tW33))
						self.pnlt += tf.reduce_mean(tf.square(self.tb33))
						# self.pnlt += tf.reduce_mean(tf.square(self.tW3))
						self.pnlt2 = tf.reduce_mean(tf.square(self.tW3)) if self.pnlt is None else  self.pnlt + tf.reduce_mean(tf.square(self.tW3))
						self.pnlt2 += tf.reduce_mean(tf.square(self.tb3))
				

				if self.orders.find('4')>-1:
					print("Order {} included".format(4))
					self.tW4 = tf.get_variable("W4",shape=[self.n_l1, 1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					self.tb4 = tf.get_variable("b4",shape=[1], dtype='float32',initializer=tf.random_uniform_initializer(0,upbound2))
					self.tZ4 = tf.matmul(self.tZ41, self.tW4) + self.tb4
					self.tZ = self.tZ4 if self.tZ is None else self.tZ4 +self.tZ
			
					if self.use_l2:
						self.pnlt = tf.reduce_mean(tf.square(self.tW41)) if self.pnlt is None else self.pnlt + tf.reduce_mean(tf.square(self.tW41))  
						self.pnlt +=  tf.reduce_mean(tf.square(self.tb41))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tW42))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tb42))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tW43))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tb43))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tW44))
						self.pnlt +=  tf.reduce_mean(tf.square(self.tb44))
						# self.pnlt +=  tf.reduce_mean(tf.square(self.tW4))
						self.pnlt2 = tf.reduce_mean(tf.square(self.tW4)) if self.pnlt is None else  self.pnlt + tf.reduce_mean(tf.square(self.tW4))
						self.pnlt2 +=  tf.reduce_mean(tf.square(self.tb4))

			# self.tZ2 = tf.layers.batch_normalization(tf.matmul(self.tZ1, self.tW2), momentum=0.9, training=self.phase)


			# self.tZ= self.tZ1 +self.tZ2 + self.tZ3 + self.tZ4
			self.err = tf.reduce_mean(tf.squared_difference(self.tZ, self.tY))

			self.acc = tf.reduce_mean(self.target_scale*tf.abs(self.tZ-self.tY))
			self.pnlt= None if self.pnlt is None else self.pnlt*1e-5
			self.dpenalty= None if self.dpenalty is None else self.dpenalty*1e-5

			# self.pnlt2 = tf.reduce_sum(
			# 	tf.squared_difference(1e-3*
			# 		tf.eye(n_l1),
			# 		tf.matmul(tf.transpose(self.tW11),self.tW12)))
			if self.pnlt is not None:
				print("Using penalty")
				self.cost = self.err + self.pnlt*self.pnlt2 #+self.pnlt2
			else:
				print("Not using penalty")
				self.cost = self.err
			if self.dpenalty is not None:
				print("Using diagonalization penalty")
				self.cost = self.err + self.dpenalty  #+self.pnlt2
			else:
				print("Not using diagonalization penalty")
				self.cost = self.err
			# self.accuracy = tf.reduce_mean(tf.abs(self.tZ - self.tY))

			self.set_new_trainer()

			# Set up a saver to save the model state
			########################################
			self.saver = tf.train.Saver()

			# Create a session to run operations
			####################################
			# self.sess = tf.Session(config=tf.ConfigProto(gpu_options=self.gpu_options))
			self.sess.run(tf.global_variables_initializer())
			self.summary_learning_rate = tf.summary.scalar('{}_learning_rate'.format(self.name),self.learning_rate)
			self.summary_cost=tf.summary.scalar('{}_cost_train'.format(self.name),self.cost)
			# self.summary_cost_b=tf.summary.scalar('{}_cost_train_b'.format(self.name),self.cost)
			self.summary_acc_valid=tf.summary.scalar('{}_cost_valid'.format(self.name),self.acc)
			self.summary_acc_train=tf.summary.scalar('{}_cost_train'.format(self.name),self.acc)
			self.writer = tf.summary.FileWriter('logs/'+ self.logname+"/",flush_secs=2,graph=self.sess.graph)


	def set_new_trainer(self,optimizer=None,lr=None,M=None,A=None,B=None,use_nesterov=None,epochs=None):
		# Optimization
		##############

		if optimizer is not None:
			self.optimizer=optimizer
		if M is not None:
			self.M=M
		if A is not None:
			self.A=A
		if B is not None:
			self.B=B
		if use_nesterov is not None:
			self.use_nesterov=use_nesterov
		if lr is not None:
			self.lr=lr
		if epochs is not None:
			self.epochs=epochs
		starter_learning_rate = self.lr
		# starter_momentum_rate = 0.9
		data_size=4*self.N/5
		global_step = tf.Variable(0, trainable=False)
		steps_per_epoch = int(data_size/self.batchsize)
		num_global_steps=int(steps_per_epoch * 300)
		decay_rate=0.9
		# momentum_rate = tf.train.exponential_decay(starter_momentum_rate, global_step,num_global_steps, decay_rate, staircase=False)
		self.learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,num_global_steps, decay_rate, staircase=False)
		# self.learning_rate = tf.train.piecewise_constant(global_step,[steps_per_epoch*1000,steps_per_epoch*2000,steps_per_epoch*3000,steps_per_epoch*4000,steps_per_epoch*5000,steps_per_epoch*6000,steps_per_epoch*7000],[1e-6,1e-5,1e-4,1e-3,1e-4,1e-5,1e-6])
		update_ops= tf.get_collection(tf.GraphKeys.UPDATE_OPS)
		self.momentum = tf.train.piecewise_constant(global_step,[steps_per_epoch*1000,steps_per_epoch*2000],[0.5,0.9,0.99])
		with tf.control_dependencies(update_ops):
			if self.optimizer=='adagrad':
				self.optimizer_step = tf.train.AdagradOptimizer(self.lr).minimize(self.cost,global_step)
			if self.optimizer=='adadelta':
				self.optimizer_step = tf.train.AdadeltaOptimizer(self.lr,self.A).minimize(self.cost,global_step)
			if self.optimizer=='rmsprop':
				self.optimizer_step = tf.train.RMSPropOptimizer(self.lr,self.M).minimize(self.cost,global_step)
			if self.optimizer=='sgd':
				self.optimizer_step = tf.train.GradientDescentOptimizer(self.lr).minimize(self.cost,global_step)
			if self.optimizer=='adam':
				self.optimizer_step = tf.train.AdamOptimizer(self.lr,self.A,self.B).minimize(self.cost,global_step)
				# self.optimizer_step = tf.train.AdamOptimizer(self.learning_rate,self.A,self.B).minimize(self.cost,global_step)
			if self.optimizer=='momentum':
				self.optimizer_step = tf.train.MomentumOptimizer(self.learning_rate,self.M,use_nesterov=self.use_nesterov).minimize(self.cost,global_step)
			if self.optimizer=='lbfgs':
				self.optimizer_step = tf.contrib.opt.ScipyOptimizerInterface(self.cost,method='L-BFGS-B',options={'eps': 1e-12})#.minimize(self.cost)
			# self.optimizer_step = tf.train.AdamOptimizer(1e-3,beta1=1e-2,beta2=0.9).minimize(self.cost,global_step)
			# self.optimizer_step = tf.train.ProximalAdagradOptimizer(1e-2).minimize(self.cost,global_step)
			# self.optimizer_step = tf.train.MomentumOptimizer(self.learning_rate,self.momentum,use_nesterov=True).minimize(self.cost,global_step)
		
		if "sess" not in self.__dict__:
			self.sess = tf.Session(config=tf.ConfigProto(gpu_options=self.gpu_options))
		# self.sess.run(tf.variables_initializer([self.optimizer_step,]))

		
	def restore(self,name):
		self.saver.restore(self.sess,"logs/{}.pkt".format(name))

	def fit(self, x, y,valid=None):
		list_val_score=[]
		list_train_score=[]

		subset_inds_4_marker=np.random.permutation(np.arange(x.shape[0]))[::5]
		for e in range(self.epochs):
			shuffle = np.random.permutation(np.arange(x.shape[0]))
			myx=x[shuffle]
			myy=y[shuffle]
			for b in np.random.permutation(range(0, x.shape[0], self.batchsize)):
			# for b in range(0, x.shape[0], self.batchsize):
				bstart = b  # *self.batchsize
				bend = b + self.batchsize
				this_x = myx[bstart:bend]
				this_y = myy[bstart:bend]
				# self.sess.run(self.optimizer_step, feed_dict={self.tX: this_x, self.tY: this_y, self.phase:True})
				# summary=self.sess.run(self`.summary_cost_b,feed_dict={self.tX: this_x, self.tY: this_y})
				# self.writer.add_summary(summary,e*int(self.epochs/self.batchsize)+int(bstart/self.batchsize))
				if self.optimizer=='lbfgs':
					self.optimizer_step.minimize(self.sess, feed_dict={self.tX: this_x, self.tY: this_y})
				else:
					self.sess.run(self.optimizer_step, feed_dict={self.tX: this_x, self.tY: this_y})
			# summary=self.sess.run(self.summary_cost,feed_dict={self.tX: x, self.tY: y,self.phase:True})
			# summary=self.sess.run(self.summary_cost,feed_dict={self.tX: x, self.tY: y})
			summary=self.sess.run(self.summary_cost,feed_dict={self.tX: x, self.tY: y})
			self.writer.add_summary(summary,e)
			summary=self.sess.run(self.summary_learning_rate)
			self.writer.add_summary(summary,e)
			if valid is not None:
				# summary=self.sess.run(self.summary_cost_valid,feed_dict={self.tX: valid[0], self.tY: valid[1], self.phase:False})
				summary=self.sess.run(self.summary_acc_valid,feed_dict={self.tX: valid[0], self.tY: valid[1]})
				self.writer.add_summary(summary,e)
				summary=self.sess.run(self.summary_acc_train,feed_dict={self.tX: x, self.tY: y})
				self.writer.add_summary(summary,e)
				# list_val_score.append(self.sess.run(self.acc,feed_dict={self.tX: valid[0], self.tY: valid[1], self.phase:False}))
				list_val_score.append(self.sess.run(self.acc,feed_dict={self.tX: valid[0], self.tY: valid[1]}))
				#it is used to identify a point in learning where the validation might have been minimal without looking at validation results
				# list_train_score.append(self.sess.run(self.acc,feed_dict={self.tX: x[subset_inds_4_marker], self.tY:y[subset_inds_4_marker],self.phase:False}))
				list_train_score.append(self.sess.run(self.acc,feed_dict={self.tX: x[subset_inds_4_marker], self.tY:y[subset_inds_4_marker]}))
				if np.isnan(list_train_score).any() or np.isnan(list_val_score).any():			
					return np.array([np.inf]),np.array([np.inf])
				if e>int(0.1*self.epochs) and list_train_score[-1]==min(list_train_score):
					self.model_save_name = "models/{}".format(self.logname)
					self.full_model_save_name = "logs/{}.pkt".format(self.model_save_name)
					print("Epoch {}:Saving model train score {} validation score {}".format(e,list_train_score[-1],list_val_score[-1]))
					print("in {}".format(self.model_save_name))
					self.saver.save(self.sess,self.full_model_save_name)
			ar_train_score=np.array(list_train_score)
			ar_val_score=np.array(list_val_score)
		return ar_train_score,ar_val_score
	
	def predict(self, x):
		pred = self.sess.run(self.tZ, feed_dict={self.tX: x})
		return (pred)


