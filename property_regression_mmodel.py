import numpy as np
from cheml.datasets import load_qm9
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from multilinear_regression import tfmodel
import os



###############################################
#####LOADING DATA
###############################################
qm9=load_qm9()

energies=None
target=os.environ['MULTILINEAR_REGRESSION_TARGET']
if target is not None:
    energies = qm9[target]
else:
    energies = qm9.T
print("Predicting {}",target)

NLIM=os.environ['NDATAPOINTS']
if NLIM is not None:
    NLIM = int(NLIM)
else:
    NLIM = energies.shape[0]
print("Predicting {}",target)
ch=3

channel_dict=dict(f=0,c=1,v=2)
n_folds = 5

fdata=None
fdata=np.load('data/results_qm9_width_2017-08-18_09-16-43_osc_[1]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X1=fdata['coef']
x1_inds=fdata['inds']
x1_inds_s=np.argsort(x1_inds)
X1=X1[x1_inds_s]
fdata=None
fdata=np.load('data/results_qm9_bonds_2017-11-21_14-19-50_osc_[1]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X1b=fdata['coef']
x1b_inds=fdata['inds']
x1b_inds_2=np.argsort(x1b_inds)
X1b=X1b#[inds]
fdata=None
fdata=np.load('data/results_qm9_val_bonds_2017-12-23_21-26-39_osc_[1]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_0.05_Ea_1e-06.npz')
X1vb=fdata['coef']
x1vb_inds=fdata['inds']
x1vb_inds_2=np.argsort(x1vb_inds)
X1vb=X1vb#[inds]


fdata=None
fdata=np.load('data/results_qm9_width_2017-08-05_09-19-18_osc_[2]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X2=fdata['coef']
x2_inds=fdata['inds']
x2_inds_s=np.argsort(x2_inds)
X2=X2[x2_inds_s]
fdata=None
fdata=np.load('data/results_qm9_bonds_2017-11-22_19-40-42_osc_[2]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X2b=fdata['coef']
x2b_inds=fdata['inds']
x2b_inds_s=np.argsort(x2b_inds)
X2b=X2b#[x2b_inds_s]
fdata=None
fdata=np.load('data/results_qm9_val_bonds_2017-12-24_08-19-40_osc_[2]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_0.05_Ea_1e-06.npz')
X2vb=fdata['coef']
x2vb_inds=fdata['inds']
x2vb_inds_s=np.argsort(x2vb_inds)
X2vb=X2vb#[x2b_inds_s]

fdata=None
fdata=np.load('data/results_qm9_width_2017-11-03_22-49-22_osc_[3]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X3=fdata['coef']
x3_inds=fdata['inds']
x3_inds_s=np.argsort(x3_inds)
X3=X3[x3_inds_s]
fdata=None
fdata=np.load('data/results_qm9_bonds_2017-11-25_20-26-02_osc_[3]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_1e-06_Ea_0.05.npz')
X3b=fdata['coef']
x3b_inds=fdata['inds']
x3b_inds_s=np.argsort(x3b_inds)
X3b=X3b#[x3b_inds_s]
fdata=None
fdata=np.load('data/results_qm9_val_bonds_2017-12-24_15-45-13_osc_[3]_sig_[1.67320285_3.34640569_6.69281139_13.38562277]_Eo_0.05_Ea_1e-06.npz')
X3vb=fdata['coef']
x3vb_inds=fdata['inds']
x3vb_inds_s=np.argsort(x3vb_inds)
X3vb=X3vb#[x3b_inds_s]

#For J=1 and J=3 the 0 order integrals (input integrals) were not computed so we concatenate them here to have the same number of dimensions as in J=3
X1=np.concatenate([X3[:,:,:1,:],X1],axis=2)
X2=np.concatenate([X3[:,:,:1,:],X2],axis=2)
X=np.concatenate([X1,X2,X3],axis=2)
Xb=np.concatenate([X1b,X2b,X3b],axis=2)
# Xvb=np.concatenate([X1vb,X2vb,X3vb],axis=2)

X=np.concatenate([X,Xb],axis=1)
# X=np.concatenate([X,Xvb],axis=1)
###############################################
#####END LOADING DATA
###############################################

###############################################
##### CREATE FOLDS FOR VALIDATION
###############################################
P=qm9.P_stratified_Ua.T
cv = [(np.concatenate(P[np.arange(n_folds) != i], axis=0), P[i]) for i in range(n_folds)]
###############################################
##### END CREATE FOLDS FOR VALIDATION
###############################################

only_order1=False
print("only_order1 {}".format(only_order1))
if only_order1:
    JJ=np.unique(fdata["sigmas"]).shape[0]
    LL=np.unique(fdata["n_oscillations"]).shape[0]
    n_l1=JJ*LL
    X=X[:,:,:n_l1]
assert(np.isnan(X).any()==False)
X=X.reshape((X.shape[0],-1))
Y=np.array(energies)
N=X.shape[0]


###########################################
#insttantiating preprocessing functions
###########################################
pca = PCA(n_components=int(X.shape[1]),whiten=False)
scaler = StandardScaler()

import pickle
def f(idxtrain,idxtest,c,optimizer='adam',lr=7*1e-5,M=0.1,A=0.9,B=0.999):
    tstamp=datetime.now().strftime('_%d%m%Y_%H%M%S')
    print("Preprocessing")
    ###########################################
    #preprocessing
    ###########################################
    print("X preprocessing")
    prepX=make_pipeline(scaler,pca)
    prepX=prepX.fit(X[idxtrain])
    print("X dumping")
    prepXpicle=pickle.dumps(prepX)
    Xtrain = prepX.transform(X[idxtrain])
    Xtest = prepX.transform(X[idxtest])

    print("Y preprocessing")
    prepY=make_pipeline(scaler)
    prepY=prepY.fit(Y[idxtrain,np.newaxis])
    prepYpicle=pickle.dumps(prepY)
    Ytrain=prepY.transform(Y[idxtrain,np.newaxis])
    Ytest=prepY.transform(Y[idxtest,np.newaxis])
    print("Y dumping")

    Ymean=prepY.get_params()['standardscaler'].mean_
    Ystd=prepY.get_params()['standardscaler'].scale_
    ###########################################
    # end preprocessing
    ###########################################

    print("Preprocessing finished")

    target_scale=Ystd
    target_offset=Ymean
    use_nesterov=False
    lr=lr
    n_l1=int(2**np.round(np.log2(Xtrain.shape[-1])))

    orders='123'
    logname="fold_{}_t_{}_l1_{}_o_{}_lr_{}_M_{}_A_{}_B_{}_ord_{}_nlim_{}".format(fold,tstamp,n_l1,optimizer,lr,M,A,B,orders,NLIM)
    # n_l1=300
    # net=pytorchmodel(N,logname,name='tfmodel_{}'.format(fold),n_observed=Xtrain.shape[-1],target_scale=Ystd,target_offset=Ymean,n_l0=Xtrain.shape[1],epochs=10000, batchsize=1024)
    print("#"*80)
    print("Instantiating Model")
    print("#"*80)
    net=tfmodel(N,logname,name='tfmodel_{}'.format(fold),epochs=5000, batchsize=128,
       optimizer=optimizer,n_l0=Xtrain.shape[-1], use_l2=False,
       n_l1=n_l1,target_scale=target_scale,target_offset=target_offset,
       M=M,A=A,B=B,use_nesterov=use_nesterov,lr=lr,orders=orders)
    # return net
    print("#"*80)
    print("Fitting Model")
    print("#"*80)
    tr,te1=net.fit(Xtrain,Ytrain,valid=[Xtest,Ytest])
    print("#"*80)
    print("Done")
    print("#"*80)
    print("Datapoints: {}, best MAE: {}".format(idxtrain.shape[0],te1[np.argmin(tr)]))

    # For early stopping we save the model with the best training error and load it at the end for evaluation.
    print("Restoring best state: {}".format(net.model_save_name))
    net.restore(net.model_save_name)
    print("State Restored!")
    Yrecon=net.predict(Xtest)
    print("Mean MAE: {}".format(np.mean(np.abs(Yrecon-Ytest))*target_scale))
    return tr,te1,net,prepXpicle,prepYpicle,Xtest,Ytest*target_scale,Yrecon*target_scale,logname,net.model_save_name


mMAEs=[]
Ytests_all=[]
Yrecons_all=[]
lnames=[]
snames=[]
for i in range(n_folds):
    fold = i
    Ytests=[]
    Yrecons=[]
    trainidx=cv[fold][0]
    trainidx=trainidx[:NLIM]
    testidx=cv[fold][1]
    l=[]
    s = []
    #################################################################################
    ### We run 5 models with different initializations to reduce prediction variance
    #################################################################################
    for j in range(5):
        print("runing model {} on fold {} of target {}".format(j,fold,target))
        net,MAE,net,prepX,prepY,Xtest,Ytest,Yrecon,logname,sname=f(trainidx,testidx,fold)
        Ytests.append(Ytest)
        Yrecons.append(Yrecon)
        l.append(logname)
        s.append(sname)
    lnames.append(l)
    snames.append(s)
    Ytests=np.array(Ytests)
    Yrecons=np.array(Yrecons)
    assert np.diff(Ytests,axis=0).sum()==0
    Yrecons=np.array(Yrecons)
    mMAEs.append(np.mean(np.abs(np.mean(Yrecons,0)-Ytests[0])))
    print("Boosted MAE: {}".format(mMAEs[-1]))
    Ytests_all.append(Ytests)
    Yrecons_all.append(Yrecons)
print('All mMAEs {}'.format(mMAEs))
print('average mMAEs {}'.format(np.mean(mMAEs)))


Ytests_all=np.array(Ytests_all)
Yrecons_all=np.array(Yrecons_all)
lnames=np.array(lnames)
sname=np.array(sname)
mMAEs=np.array(mMAEs)
np.savez("predictions_on_{}_NLIM_{}.npz".format(target,NLIM),partitions=P,Ytests=Ytests_all,Yrecons=Yrecons_all,mMAEs=mMAEs,logname=logname,sname=sname)
