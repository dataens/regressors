# Multilinear regression for Solid Harmonic Scattering coefficients

This repository holds the scripts used for the regressions of molecular properties in [Solid Harmonic Wavelet Scattering: Predicting Quantum Molecular Energy from Invariant Descriptors of 3D Electronic Densities](https://papers.nips.cc/paper/7232-solid-harmonic-wavelet-scattering-predicting-quantum-molecular-energy-from-invariant-descriptors-of-3d-electronic-densities)

## Getting Started

The code was adapted to run on a cluster suing slurm

### Prerequisites

The code is built on top of scikit-learn, tensorflow, and numpy. Additionally, you need to install the [CheML](https://github.com/CheML/CheML) package to obtain information about the molecules.

```
$ git clone https://github.com/CheML/CheML.git
$ cd CheML 
$ python setup.py install
$ pip install numpy
$ pip install tensorflow-gpu
$ pip install  scikit-learn
```

## Running the code

You can run the code on a slurm enabled cluster with the desired target by uncommenting the corresponding line in bat.sh using 
```
$ sbatch bat.sh
```


