#!/bin/bash
#SBATCH --output=scattering-%j.out
#SBATCH --error=scattering-%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=10000M
#SBATCH --mail-user=georgios.exarchakis@uni-oldenburg.de
#SBATCH --mail-type=FAIL
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --job-name="BSC"
#SBATCH --partition=gold
#SBATCH --time=168:00:00
#SBATCH --gres=gpu:titanx:1
##SBATCH --nodelist=gold0[8-9],gold10


source /etc/profile.d/modulecmd-gold-env.sh

#Here should be the command you want to execute. For example :
module load cuda
#module load cudnn/8.0-v5.1
module load cudnn/8.0-v6.0
#srun python batch_processing.py $SLURM_ARRAY_TASK_ID 4
#srun $HOME/miniconda3/envs/local/bin/python batch_processing.py $SLURM_ARRAY_TASK_ID 10

#srun $HOME/miniconda3/envs/local/bin/python qm9_regression_multimodel.py

LD_LIBRARY_PATH=/users/ml/xoex6879/miniconda3/envs/local/lib:$LD_LIBRARY_PATH
#export MULTILINEAR_REGRESSION_TARGET=Ua
export MULTILINEAR_REGRESSION_TARGET=U0a
#export MULTILINEAR_REGRESSION_TARGET=Ha
#export MULTILINEAR_REGRESSION_TARGET=Ga
#export MULTILINEAR_REGRESSION_TARGET=alpha
#export MULTILINEAR_REGRESSION_TARGET=Cv
#export MULTILINEAR_REGRESSION_TARGET=gap
#export MULTILINEAR_REGRESSION_TARGET=lumo
#export MULTILINEAR_REGRESSION_TARGET=homo
#export MULTILINEAR_REGRESSION_TARGET=mu
#export MULTILINEAR_REGRESSION_TARGET=R2
#export MULTILINEAR_REGRESSION_TARGET=zpve
export NDATAPOINTS=30000
srun $HOME/miniconda3/envs/local/bin/python property_regression_mmodel.py


#srun python qm9_regression_multimodel.py
#srun /users/ml/xuze0554/Projects/bin/main --parameter 3
#mpirun python main.py arg1 arg2
#etc.
